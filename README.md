# Практика #3 CSS-селекторы
* Презентация с детальным описанием заданий https://slides.com/frontschool-nsk/css#/
## Задание #2
* Сверстать макет страницы в файле task-2.html
* Макеты в фигме https://www.figma.com/file/gBhtWYRgq4cxSvbcJCswHo/For-frontenders-3
* Макеты, описывающие взаимодействие с меню и всплывающее окно - в папке layout
* Палитра цветов https://guides.kontur.ru/resources/colors/
* Тогл https://guides.kontur.ru/components/toggle/
